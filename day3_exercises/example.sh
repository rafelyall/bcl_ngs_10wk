#!/bin/bash

echo "Provide two numbers and I will add them!"

total_arguments=$#
number_one=$1
number_two=$2

if [ $total_arguments == 0 ]
then
    echo "ERROR: You gave me no numbers."
elif [ $total_arguments == 1 ]
then
    echo "ERROR: You gave me only one number."
else
    sum=$(($number_one+$number_two))
    echo "SUCCESS: The sum is: $sum"
fi
